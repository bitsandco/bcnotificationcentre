//
//  BCNotificationCentreTests.swift
//  BCNotificationCentreTests
//
//  Created by Romain Francez on 08/05/2016.
//  Copyright © 2016 Bits & Co. All rights reserved.
//

import XCTest

@testable import BCNotificationCentre

class BCNotificationCentreTests: XCTestCase {
    
    fileprivate var sut: BCNotificationCentre!
    
    override func setUp() {
        super.setUp()
        
        sut = BCNotificationCentre()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPostNotification() {
        // Given
        let notificationName = "BCNotificationName"
        let notification = BCNotification<String, Any>(name: notificationName)
        
        // When
        sut.post(notification)
        
        // Then
        // Expect nothing
    }
    
    func testAddObserverForNameAndRecieveNotification() {
        // Given
        let notificationName = "BCNotificationName"
        let notification = BCNotification<String, Any>(name: notificationName)
        var recievedNotification: BCNotification<String, Any>?
        let asyncNotificationExpectation = expectation(description: "asyncNotificationExpectation")
        let notificationBlock = { (notification: BCNotification<String, Any>) -> Void in
            recievedNotification = notification
            asyncNotificationExpectation.fulfill()
        }
        
        // When
        sut.addObserver(forName: notificationName, using: notificationBlock)
        sut.post(notification)
        
        // Then
        waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error)
            XCTAssertNotNil(recievedNotification)
            XCTAssertEqual(notification.name, recievedNotification?.name)
        }
    }
    
    func testAddObserverForNameWithPriorityAndRecieveNotification() {
        // Given
        let notificationName = "BCNotificationName"
        let notification = BCNotification<String, Any>(name: notificationName)
        var recievedNotification: BCNotification<String, Any>?
        let asyncNotificationExpectation = expectation(description: "asyncNotificationExpectation")
        let notificationBlock = { (notification: BCNotification<String, Any>) -> Void in
            recievedNotification = notification
            asyncNotificationExpectation.fulfill()
        }
        
        // When
        sut.addObserver(forName: notificationName, priority: .low, using: notificationBlock)
        sut.post(notification)
        
        // Then
        waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error)
            XCTAssertNotNil(recievedNotification)
            XCTAssertEqual(notification.name, recievedNotification?.name)
        }
    }
    
    func testAddObserverForNameWithQueueAndRecieveNotification() {
        // Given
        let notificationName = "BCNotificationName"
        let notification = BCNotification<String, Any>(name: notificationName)
        var recievedNotification: BCNotification<String, Any>?
        let asyncNotificationExpectation = expectation(description: "asyncNotificationExpectation")
        let notificationBlock = { (notification: BCNotification<String, Any>) -> Void in
            recievedNotification = notification
            asyncNotificationExpectation.fulfill()
        }
        
        // When
        sut.addObserver(forName: notificationName, queue: OperationQueue.main, using: notificationBlock)
        sut.post(notification)
        
        // Then
        waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error)
            XCTAssertNotNil(recievedNotification)
            XCTAssertEqual(notification.name, recievedNotification?.name)
        }
    }
    
    func testAddObserverForNameWithPriorityAndQueueAndRecieveNotification() {
        // Given
        let notificationName = "BCNotificationName"
        let notification = BCNotification<String, Any>(name: notificationName)
        var recievedNotification: BCNotification<String, Any>?
        let asyncNotificationExpectation = expectation(description: "asyncNotificationExpectation")
        let notificationBlock = { (notification: BCNotification<String, Any>) -> Void in
            recievedNotification = notification
            asyncNotificationExpectation.fulfill()
        }
        
        // When
        sut.addObserver(forName: notificationName, priority: .high, queue: OperationQueue.main, using: notificationBlock)
        sut.post(notification)
        
        // Then
        waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error)
            XCTAssertNotNil(recievedNotification)
            XCTAssertEqual(notification.name, recievedNotification?.name)
        }
    }
    
    func testPriorityNotification() {
        // Given
        let notificationName = "BCNotificationName"
        let notification = BCNotification<String, Any>(name: notificationName)
        
        var recievedNotificationHighPriority: BCNotification<String, Any>?
        var recievedNotificationNormalPriority: BCNotification<String, Any>?
        var recievedNotificationLowPriority: BCNotification<String, Any>?
        
        let asyncNotificationHighPriorityExpectation = expectation(description: "asyncNotificationHighPriorityExpectation")
        let notificationHighPriorityBlock = { (notification: BCNotification<String, Any>) -> Void in
            XCTAssertNil(recievedNotificationNormalPriority)
            XCTAssertNil(recievedNotificationLowPriority)
            recievedNotificationHighPriority = notification
            asyncNotificationHighPriorityExpectation.fulfill()
        }
        
        let asyncNotificationNormalPriorityExpectation = expectation(description: "asyncNotificationNormalPriorityExpectation")
        let notificationNormalPriorityBlock = { (notification: BCNotification<String, Any>) -> Void in
            XCTAssertNotNil(recievedNotificationHighPriority)
            XCTAssertNil(recievedNotificationLowPriority)
            recievedNotificationNormalPriority = notification
            asyncNotificationNormalPriorityExpectation.fulfill()
        }
        
        
        let asyncNotificationLowPriorityExpectation = expectation(description: "asyncNotificationLowPriorityExpectation")
        let notificationLowPriorityBlock = { (notification: BCNotification<String, Any>) -> Void in
            XCTAssertNotNil(recievedNotificationHighPriority)
            XCTAssertNotNil(recievedNotificationNormalPriority)
            recievedNotificationLowPriority = notification
            asyncNotificationLowPriorityExpectation.fulfill()
        }
        
        // When
        sut.addObserver(forName: notificationName, priority: .high, queue: OperationQueue.main, using: notificationHighPriorityBlock)
        sut.addObserver(forName: notificationName, priority: .low, queue: OperationQueue.main, using: notificationLowPriorityBlock)
        sut.addObserver(forName: notificationName, priority: .normal, queue: OperationQueue.main, using: notificationNormalPriorityBlock)
        sut.post(notification)
        
        // Then
        waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error)
            XCTAssertNotNil(recievedNotificationHighPriority)
            XCTAssertNotNil(recievedNotificationLowPriority)
            XCTAssertEqual(notification.name, recievedNotificationHighPriority?.name)
            XCTAssertEqual(recievedNotificationHighPriority?.name, recievedNotificationNormalPriority?.name)
            XCTAssertEqual(recievedNotificationHighPriority?.name, recievedNotificationLowPriority?.name)
        }
    }
    
    func testTypedNotifications() {
        // Given
        let notificationName = "BCNotificationName"
        let notification = BCNotification(name: notificationName, value: 1)
        
        let asyncNotificationExpectation = expectation(description: "asyncNotificationExpectation")
        
        var recievedGoodTypedNotification: BCNotification<String, Int>?
        let goodTypedNotificationBlock = { (notification: BCNotification<String, Int>) -> Void in
            recievedGoodTypedNotification = notification
            asyncNotificationExpectation.fulfill()
        }
        
        var recievedBadTypedNotification: BCNotification<String, String>?
        let badTypedNotificationBlock = { (notification: BCNotification<String, String>) -> Void in
            recievedBadTypedNotification = notification
            asyncNotificationExpectation.fulfill()
        }
        
        // When
        sut.addObserver(forName: notificationName, priority: .low, using: goodTypedNotificationBlock)
        sut.addObserver(forName: notificationName, priority: .high, using: badTypedNotificationBlock)
        sut.post(notification)
        
        // Then
        waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error)
            XCTAssertNotNil(recievedGoodTypedNotification)
            XCTAssertNil(recievedBadTypedNotification)
            XCTAssertEqual(notification.name, recievedGoodTypedNotification?.name)
            XCTAssertEqual(notification.value, recievedGoodTypedNotification?.value)
        }
    }
    
    func testDefaultCentre() {
        let sut = BCNotificationCentre.default
        XCTAssertEqual(ObjectIdentifier(BCNotificationCentre.default), ObjectIdentifier(sut))
        XCTAssertNotEqual(ObjectIdentifier(BCNotificationCentre()), ObjectIdentifier(sut))
    }
    
}

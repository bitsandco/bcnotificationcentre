//
//  BCNotification.swift
//  BCNotificationCentre
//
//  Created by Romain Francez on 08/05/2016.
//  Copyright © 2016 Bits & Co. All rights reserved.
//

import Foundation

public struct BCNotification<T, U>: BCNamedNotification, BCValuedNotification where T: Hashable {
    
    /// Name of the notification
    public let name: T
    
    /// Optional value of the notification
    public let value: U?
    
    public init(name: T, value: U? = nil) {
        self.name = name
        self.value = value
    }
    
}

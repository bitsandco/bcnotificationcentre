//
//  BCValuedNotification.swift
//  BCNotificationCentre
//
//  Created by Romain Francez on 15/05/2016.
//  Copyright © 2016 Bits & Co. All rights reserved.
//

import Foundation

public protocol BCValuedNotification {
    
    associatedtype U
    
    var value: U? { get }
    
}

/*extension Notification: BCValuedNotification {
    
    public var value: [AnyHashable : Any]? {
        return userInfo
    }
    
}*/

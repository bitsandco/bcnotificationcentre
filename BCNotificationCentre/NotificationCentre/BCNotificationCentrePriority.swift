//
//  BCNotificationCentrePriority.swift
//  BCNotificationCentre
//
//  Created by Romain Francez on 08/05/2016.
//  Copyright © 2016 Bits & Co. All rights reserved.
//

import Foundation

public enum BCNotificationCentrePriority: Int {
    case low = -4
    case normal = 0
    case high = 4
}

extension BCNotificationCentrePriority: Comparable { }

public func <(lhs: BCNotificationCentrePriority, rhs: BCNotificationCentrePriority) -> Bool {
    return lhs.rawValue < rhs.rawValue
}
